# Elastix Installer

Un par de scripts para automatizar la instalación de Elastix 4 sobre una
instalación existente de CentOS 7 Minimal completamente actualizado

### Prerrequisitos

Este conjunto de scripts funciona en una instalación base de CentOS 7
Minimal. Dicho servidor debe contar con una salida habilitada a Internet
antes de iniciar el proceso de instalación

### Instalación

Descargue los dos stages del script, y haga los mismos ejecutables

```
curl -LO https://gitlab.com/agtech/issabel-installer/raw/master/elastix-installer-stage1
curl -LO https://gitlab.com/agtech/issabel-installer/raw/master/elastix-installer-stage2
chmod +x elastix-installer-stage1
chmod +x elastix-installer-stage1
```
Ejecute el Stage 1 de instalación

```
sudo ./elastix-installer-stage1
```

Después de reiniciar, ejecute el Stage 2 de instalación

```
sudo ./elastix-installer-stage2
```

## Autores

* **Eduar Cardona (ecardona@aloglobal.com)** - *Código Inicial*
